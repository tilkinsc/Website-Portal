<?php

use App\Http\PlayerController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/playerexport/export', [PlayerController::class, 'exportSubmitApi'])->middleware(['throttle:15,20'])->name('PlayerExportSubmitApi');

Route::post('/register', [PlayerController::class, 'registerUserApi'])->middleware(['throttle:15,20'])->name('RegisterUserApi');